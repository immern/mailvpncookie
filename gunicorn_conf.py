import os

bind = "0.0.0.0:13300"
workers = 2
worker_class = 'tornado'
timeout = 300

name = 'mailvpncookie'
raw_env = ['DJANGO_SETTINGS_MODULE=mailvpncookie.settings']

chdir = os.path.abspath(os.path.dirname(__file__))
logdir = os.path.join(chdir, 'logs')
pidfile = os.path.join(logdir, 'gunicorn_server.pid')

user = 'root'
group = 'root'

daemon = True
