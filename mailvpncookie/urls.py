from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from views import *
from cloudlogin.views import requires_info

urlpatterns = patterns('',
                    #    Examples:
                    #    url(r'^$', requires_info(home)),  
                     
                        #url(r'^blog/', include('blog.urls')),
                        #url(r'^admin/', include(admin.site.urls)),
                        
                        url(r'^newvpn/$', requires_info(newvpn)),
                        url(r'^delete/$', requires_info(delete)),
                        url(r'^down/$', requires_info(down)),
                        
                        url(r'^help/', include('cloudhelp.urls')),
                        url(r'^uad/', include('uad.urls')),
                        url(r'^', include('cloudlogin.urls')),
                        url(r'^home/$', 'mailvpncookie.views.home'),
)
