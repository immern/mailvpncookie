from django.db import models

# Create your models here.

class VPNlinker(models.Model):
    id = models.AutoField(primary_key=True)
    vpnname = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    host = models.CharField(max_length=25)
    user = models.CharField(max_length=25)
    password = models.CharField(max_length=25)
    processinfo = models.CharField(max_length=1024)
    vpnremark = models.CharField(max_length=255)
    vpnsystemurl = models.CharField(max_length=255)
