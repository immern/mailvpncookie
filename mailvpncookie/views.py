# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from models import VPNlinker

import os, re, subprocess, time

from cloudutils.utils import handle_download_file   

#views
def home(request):
    ''' 显示VPN生成器 '''
    
    vpns_list = VPNlinker.objects.order_by('-id').all()
    paginator = Paginator(vpns_list, 10)
    page = request.GET.get("page","1")
    
    try:
        vpns = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        vpns = paginator.page(1)
    except EmptyPage:
        page = paginator.num_pages
        vpns = paginator.page(paginator.num_pages)    
    countpage = paginator.num_pages  
    
    return render_to_response('vpnlinker.html',locals())


def newvpn(request):
    ''' 生成VPN生成器 '''
    
    vpnname = request.GET.get('vname','')
    description = request.GET.get('vdes','')
    host = request.GET.get('vip','')
    user = request.GET.get('vuser','')
    password = request.GET.get('vpass','')
    vpnremark = request.GET.get('vvt','')
    processinfo = request.GET.get('vprocess','')
    vpnsystemurl = request.GET.get('vurl','')
    
    if processinfo:
        processinfo = re.compile(r'[^0-9a-zA-Z.,_]').sub('', processinfo)
        processinfo = ",".join(filter(lambda x: len(x)>0, processinfo.split(',')))

    if host and user and password:
        VPNlinker(vpnname=vpnname, description=description, host=host, user=user, password=password, processinfo=processinfo, vpnremark=vpnremark, vpnsystemurl=vpnsystemurl).save()
    return redirect('/home/')


def delete(request):
    ''' 删除VPN生成器 '''
    
    try:
        vid = request.GET.get('id', '')
        VPNlinker.objects.get(id=vid).delete()
        return HttpResponse("success")
    except Exception,e:
        print e
    return HttpResponse("fail")

def dowhile(p):
    while True:
        pp = p.poll()
        if pp is not None: break
        time.sleep(1)
    
def down(request):
    ''' 下载VPN生成器 '''
    
    vid = request.GET.get('id')
    type = request.GET.get('type')
    vpnfilename = {"vbs": "vpn.vbs", "exe": "vpn.vbs", "hta": "vpn.hta"}

    try:
        if vid and (type in vpnfilename.keys()):
            vlink = VPNlinker.objects.get(id=vid)            
            with open(os.path.join(os.path.dirname(__file__), vpnfilename[type])) as f:
                filecontent = f.read()
                if type == "hta":
                    filecontent = filecontent.replace('{vname}', vlink.vpnname.encode('utf-8'))
                    filecontent = filecontent.replace('{vdes}', vlink.description.encode('utf-8'))
                    filecontent = filecontent.replace('{vip}', vlink.host.encode('utf-8'))
                    filecontent = filecontent.replace('{vuser}', vlink.user.encode('utf-8'))
                    filecontent = filecontent.replace('{vpass}', vlink.password.encode('utf-8'))
                elif type == "vbs":
                    filecontent = filecontent.replace('{host}', vlink.host.encode('utf-8'))
                    filecontent = filecontent.replace('{user}', vlink.user.encode('utf-8'))
                    filecontent = filecontent.replace('{password}', vlink.password.encode('utf-8'))
                    filecontent = filecontent.replace('{process}', vlink.processinfo.encode('utf-8'))
                elif type == "exe":
                    try:
                        filecontent = filecontent.replace('{host}', vlink.host.encode('utf-8'))
                        filecontent = filecontent.replace('{user}', vlink.user.encode('utf-8'))
                        filecontent = filecontent.replace('{password}', vlink.password.encode('utf-8'))
                        filecontent = filecontent.replace('{process}', vlink.processinfo.encode('utf-8'))
                        
                        with open(os.path.join(os.path.dirname(__file__), 'exe', 'vpn.vbs'), "w+") as f2:
                            f2.write(filecontent)
                            
                        os.chdir(os.path.join(os.path.dirname(__file__), 'exe'))
                        
                        if os.path.exists(os.path.join(os.path.dirname(__file__), 'exe', 'vpn.exe')):
                            os.remove(os.path.join(os.path.dirname(__file__), 'exe', 'vpn.exe'))
                        
                        p = subprocess.Popen('rar a -sfxDefault.SFX -zcomment.txt vpn.exe vpn.vbs', shell=True, stdout=subprocess.PIPE)
                        dowhile(p)

                        try:
                            if vlink.vpnname:
                                filename = vlink.vpnname+"."+type
                            else:
                                filename = "vpn.exe"
                        except Exception, e:
                            print e
                            filename = "vpn.exe"
                        
                        return handle_download_file(os.path.join(os.path.dirname(__file__), 'exe', 'vpn.exe'), filename)
                    except Exception,e:
                        print e
                        return HttpResponse("")
                                   
                response = HttpResponse(filecontent, mimetype='application/plain')
                try:
                    if vlink.vpnname:
                        filename = vlink.vpnname+"."+type
                        filename = filename.encode('utf-8')
                    else:
                        filename = vpnfilename[type]
                except Exception, e:
                    print e
                    filename = vpnfilename[type]

                response['Content-Disposition'] = 'attachment; filename=' + filename
                return response
    except Exception,e:
        print e
    return HttpResponse("")