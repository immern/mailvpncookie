#!/bin/bash
PROJECT_NAME=mailvpncookie

filepath=$(cd "$(dirname "$0")"; pwd)
cd $filepath

start() {
    echo "gunicorn start..."
    if [ -e gunicorn_conf.py ];then
        if ! /usr/local/bin/gunicorn ${PROJECT_NAME}.wsgi:application -c gunicorn_conf.py
        then
            if [ $(ps -ef |grep ${PROJECT_NAME}.wsgi |wc -l) -le 2 ]
            then
                ps -ef |grep ${PROJECT_NAME}.wsgi |awk '{print $2}' |xargs kill -9
                if [ -e logs/gunicorn_server.pid ]
                then
                    rm logs/gunicorn_server.pid
                fi
            fi
        fi
    fi
}

stop() {
    echo "gunicorn stop..."
    cat logs/gunicorn_server.pid |xargs kill -9
    ps -ef |grep ${PROJECT_NAME}.wsgi |awk '{print $2}' |xargs kill -9
}

case "$1" in
start)
    start
    ;;
stop)
    stop
    ;;
restart)
    stop
    start
    ;;
*)
    echo $"Usage: $0 {start|stop|restart}"
    exit 1
esac

